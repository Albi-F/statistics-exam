# CLEANING UP
# Do this first so errors from imported libraries are not erased
# Clear the terminal output
cat("\014")
# Close all open graphical devices (Clear the plots)
graphics.off()
# Clear variables existing in memory
rm(list = ls())

# Set working directory programmatically (Work only inside RStudio)
library(rstudioapi)
# Color the text
library(crayon)
# Correlation scatterplots and histograms together
library(psych)
# Print nicer tables from dataframes
library(knitr)
# Split column data into separate columns
library(tidyr)

# Get the path of your current open file
current_path = rstudioapi::getActiveDocumentContext()$path 
# And set the working directory to match it
setwd(dirname(current_path ))

# Set the color of the headers
color_header <- crayon::green

# Read the tab-separated file into a data frame
raw_data <- read.table("exam2023_data.csv", header = TRUE, sep = ",")
cat("Unique values of the column 'Season':\n", paste(unique(raw_data$Season), collapse = '\n'))
# Split the column into two based on space
clean_data <- separate(raw_data, Season, into = c("Season", "Year"), sep = " ")
clean_data <- clean_data[, !colnames(clean_data) %in% c("SurveyID")]
clean_data$Season = as.factor(clean_data$Season)
clean_data$Year = as.factor(clean_data$Year)
clean_data$Property = as.factor(clean_data$Property)
clean_data$Aspect = as.factor(clean_data$Aspect)
clean_data$Landscape.position = as.factor(clean_data$Landscape.position)
# Assuming 'Date' is the column name in your data frame
clean_data$Date <- as.Date(clean_data$Date, format = "%d/%m/%Y")

# Create a table with variable names and types
variables_type <- data.frame(stack(sapply(clean_data, class)))
# Rename the columns
colnames(variables_type) <- c("Type", "Variable")
# Print a formatted table
kable(variables_type, format = "markdown")

# Identify non-factor variables
non_factor_vars <- sapply(clean_data, function(x) is.integer(x))
non_factors_data <- clean_data[, non_factor_vars]
# Use pairs only for non-factor variables
par(mar = c(0, 0, 0, 0))
pairs.panels(non_factors_data)

# Calculate the correlation matrix
cor_matrix <- cor(non_factors_data)

# Set your minimum correlation value (e.g., 0.3 for positive or -0.3 for negative)
min_correlation <- 0.3

# Create pairs plot with only significant correlations
pairs.panels(cor_matrix[abs(cor_matrix) >= min_correlation])

# Create a boxplot for Wing_area and Body_length
boxplot(data$Wing_area, data$Body_length, names = c("Wing area", "Body length"), col = c("blue", "green"), ylab = "Value")
# Function to create named list for min and max values
min_max_list <- function(var, outliers) {
  range_values <- range(var[!var %in% outliers])
  list(min = range_values[1], max = range_values[2])
}
